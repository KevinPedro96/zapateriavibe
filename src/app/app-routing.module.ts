import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Componentes de administrador
import { CarouselAdminComponent } from './components/admin/carousel-admin/carousel-admin.component';
import { InventoryAdminComponent } from './components/admin/inventory-admin/inventory-admin.component';
import { OrdersAdminComponent } from './components/admin/orders-admin/orders-admin.component';
import { SalesAdminComponent } from './components/admin/sales-admin/sales-admin.component';
import { ShowComponent } from './components/admin/administrator/show/show.component';
// Componentes de la zapatería
import { PageInitComponent } from './components/page-init/page-init.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { MayoristasComponent } from './components/mayoristas/mayoristas.component';
import { NinaComponent } from './components/nina/nina.component';
import { DamaComponent } from './components/dama/dama.component';




const routes: Routes = [
  {path: '', component: PageInitComponent},
  {path: 'login', component: LogInComponent},
  {path: 'carrusel', component: CarouselAdminComponent},
  {path: 'inventario', component: InventoryAdminComponent},
  {path: 'pedidos', component: OrdersAdminComponent},
  {path: 'ventas', component: SalesAdminComponent},
  {path: 'administrador', component: ShowComponent},
  {path: 'mayoristas', component: MayoristasComponent},
  {path: 'niña', component: NinaComponent},
  {path: 'dama', component: DamaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,
  CommonModule]
})
export class AppRoutingModule { }
