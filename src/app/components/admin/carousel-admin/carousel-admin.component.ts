import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { GeneralService } from 'src/app/services/general.service';

class ImagenClass {
  id?: Number;
  img: String;
  position: Number;
  flag_show: Number;
}

@Component({
  selector: 'app-carousel-admin',
  templateUrl: './carousel-admin.component.html',
  styleUrls: ['./carousel-admin.component.css']
})
export class CarouselAdminComponent implements OnInit {

  arrayImg: any = [];
  base64textString: any = [];
  typeOfImg: string;
  position: number;
  flagOption: boolean = false;
  flagAnswerOfUpLoadImg: boolean = false;

  constructor(private generalService: GeneralService) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = true;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
    this.base64textString = [];
    this.getImgs();
  }

  // Mostrar imagenes cargadas
  getImgs() {
    this.generalService.getImgs('Carousel').subscribe(res => {
      this.arrayImg = res as ImagenClass[];
      this.position = this.arrayImg.length;
    });
  }
  // Cargar imagenes
  onUploadChange(evt: any) {
    const file = evt.target.files;
    if (file) {
      for (const iterator of file) {
        this.typeOfImg = iterator.type;

        const reader = new FileReader();

        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(iterator);
      }
    }
  }
  handleReaderLoaded(e) {
    this.position = this.position+1;
    let data = `data:${this.typeOfImg};base64,`;
    let imagen = {
      img: data + btoa(e.target.result),
      position: this.position,
      typeImg: 'Carousel'
    }
    this.base64textString.push(imagen);
  }
  // Mostrar imagenes en drag and drop
  showImgDD(img: any) {
    let blob = new Blob([new Uint8Array(img.data)]);
    return URL.createObjectURL(blob);
  }
  // Botón de cambio de ventana
  btnUpload(no: number) {
    if(no === 0) this.flagOption = false;
    else this.flagOption = true;
  }
  // Botón de eliminar imagen antes de cargar
  btnDeleteUploadImg(index) {
    this.base64textString.splice(index, 1);
  }
  // Botón de Guardar imagenes cargadas
  btnSaveUploadImg() {
    this.flagAnswerOfUpLoadImg = true;

    this.generalService.addImg(this.base64textString).subscribe(res => {
      this.flagAnswerOfUpLoadImg = false;
      this.base64textString = [];
      this.getImgs();
      // console.log('Respuesta: ',res);
    });
  }
  // Botón de eliminar imagen del carrusel
  btnDeleteImgCarousel(id: number) {
    this.generalService.deleteImg(id).subscribe(res => {
      this.getImgs();
      console.log(res);
    });
  }
  // Función del Drag&Drop
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.arrayImg, event.previousIndex, event.currentIndex);
    // Acomodar arreglo
    for(let i=0; i< this.arrayImg.length; i++){
      this.arrayImg[i].position = i+1;
    }
    // Guardar cambios en la base de datos
    this.generalService.editPosition(this.arrayImg).subscribe(res => {
      console.log(res);
    });
  }
}
