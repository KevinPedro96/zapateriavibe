import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPorductsComponent } from './show-porducts.component';

describe('ShowPorductsComponent', () => {
  let component: ShowPorductsComponent;
  let fixture: ComponentFixture<ShowPorductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowPorductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPorductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
