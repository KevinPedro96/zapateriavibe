import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterPorductComponent } from './register-porduct.component';

describe('RegisterPorductComponent', () => {
  let component: RegisterPorductComponent;
  let fixture: ComponentFixture<RegisterPorductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterPorductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterPorductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
