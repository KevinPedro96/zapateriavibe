import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StockService } from 'src/app/services/stock.service';

export interface Colors {
  id?: number;
  name: string;
  color: string;
}

export interface Imagen {
  id?: number;
  img: string;
  position?: number;
  typeImg: string;
}

@Component({
  selector: 'app-register-porduct',
  templateUrl: './register-porduct.component.html',
  styleUrls: ['./register-porduct.component.css']
})
export class RegisterPorductComponent implements OnInit {

  @Input() isLinear = true; // Si es true las secciones estan bloqueadas de lo contrario estan habilitadas
  // FormGroups
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  // Variables
  typeOfImg: string;
  sizeFootwaerModal: string = '';
  totalFotwaerModal: string = '';
  position: number = -1;
  // Banderas
  flagOfAddOrEdit: boolean; // si es false es añadir nuevo registro, True editar registro
  flagOfStock: boolean = false;
  flagOfImgPrincipal: boolean = false;
  flagToShowImg: boolean = false;
  flagInputFile: boolean = false;
  flagOfRefreshInputFile: boolean = true;
  flagNewColor: boolean = false;
  flagNewMaterial: boolean = false;
  flagNewTypeOfFootwaer: boolean = false;
  // Variables any
  printCategory: any = null;
  printMaterial: any = null;
  dataOfOptions: any;
  dataOfStock: any;
  optionsOfColors: Colors[] = [];
  optionsOfMaterial: any = [];
  optionsOfTypeOfFootwaer: any = [];
  arrayColor: any = [];

  constructor(private _formBuilder: FormBuilder, private stockService: StockService) {}

  ngOnInit() {
    this.getDataOfOptions();
    this.initialFirstFormGroup();
    this.initialSecondFormGroup();
    this.initialThirdFormGroup();
  }
  // Inicialización de formulario no 1
  initialFirstFormGroup() {
    this.firstFormGroup = this._formBuilder.group({
      number_folio: ['', [Validators.required, Validators.pattern('^[0-9]+')]]
    });
  }
  // Inicialización de formulario no 2
  initialSecondFormGroup() {
    this.secondFormGroup = this._formBuilder.group({
      number_folio: [this.firstFormGroup.get('number_folio').value],
      model: ['', [Validators.required]],
      name: ['', [Validators.required]],
      price: [ [Validators.required]],
      description: ['', [Validators.required]],
      id_material: ['', [Validators.required]],
      type_of_material: [''],
      id_type_footwaer: ['', [Validators.required]],
      name_type_footwaer: [''],
      img_principal: ['', [Validators.required]],
      id_img_principal: [0]
    });
  }
  // Inicialización de formulario no 3
  initialThirdFormGroup() {
    this.thirdFormGroup = this._formBuilder.group({
      selectColor: [''],
      colorname: [''],
      colorHexa: ['']
    });
  }
  // Obtener Datos para las opciones
  getDataOfOptions() {
    this.stockService.getDataGeneral().subscribe(res => {
      this.dataOfOptions = res;
      this.optionsOfColors = this.dataOfOptions.colors;
      this.optionsOfMaterial = this.dataOfOptions.material;
      this.optionsOfTypeOfFootwaer = this.dataOfOptions.typeOfMaterial;
    });
  }
  // Obtener Datos del stock
  getDataOfStock() {
    const number_folio = this.firstFormGroup.get('number_folio').value;
    this.stockService.getDataStock(Number(number_folio)).subscribe(res => {
      this.dataOfStock = res;
      this.flagOfAddOrEdit = this.dataOfStock.flag;
      // console.log(this.flagOfAddOrEdit);
      if(this.flagOfAddOrEdit) {
        // Cargar todos los valores
      }
    });
  }
  // Función del selector de colores (ubicado en el formulario no 3)
  selectColor() {
    const selectColor = this.thirdFormGroup.get('selectColor').value;

    if(selectColor == 0) {
      this.flagNewColor = true;
    } else {
      this.flagNewColor = false;
      let color = {
        data: this.optionsOfColors.find(element => element.id == selectColor),
        image: [],
        stock: []
      }
      if(this.arrayColor.length !== 0) {
        let findColor = this.arrayColor.find(element => element.data.name == color.data.name);
        if(findColor === undefined) this.arrayColor.push(color);
        else { }//poner mensaje de color registrado
      } else {
        this.arrayColor.push(color);
      }
      this.initialThirdFormGroup();
      // console.log(this.arrayColor);
    }
  }
  // Función de selector de material del calzado
  selectMaterial() {
    const selectMaterial = this.secondFormGroup.get('id_material').value;
    if(selectMaterial == 0) {
      this.flagNewMaterial = true;
      this.printMaterial = null;
    } else {
      this.flagNewMaterial = false;
      this.printMaterial = this.optionsOfMaterial.find(element => element.id == selectMaterial);
    }
  }
  // Función de selector de tipo de calzdo
  selectTypeOfFootwaer() {
    const selectTOF = this.secondFormGroup.get('id_type_footwaer').value;
    if(selectTOF == 0) {
      this.flagNewTypeOfFootwaer = true;
      this.printCategory = null;
    } else {
      this.flagNewTypeOfFootwaer = false;
      this.printCategory = this.optionsOfTypeOfFootwaer.find(element => element.id == selectTOF);
    }
  }
  // Función del checkbox
  checkboxColor(index: any, flag?) {
    this.position = index;

    if(flag) {
      this.flagInputFile = true;
      this.flagToShowImg = true;
  
      this.flagOfRefreshInputFile = false;
      setTimeout(() => {this.flagOfRefreshInputFile = true;}, 20);
    } else {
      this.sizeFootwaerModal = '';
      this.totalFotwaerModal = '';
      this.flagOfStock = true;
    }
  }
  // Función de añador nuevo color (ubicado en el formulario no 3)
  btnAddNewColor() {
    const name_color = this.thirdFormGroup.get('colorname').value;
    const color_hexa = this.thirdFormGroup.get('colorHexa').value;
    let newColor = {
      id: 0,
      name: name_color,
      color: color_hexa
    };
    let color = {
      data: newColor,
      image: [],
      stock: []
    };
    this.arrayColor.push(color);
    this.flagNewColor = false;
    this.initialThirdFormGroup();
  }
  // Función para añadir registro a astock
  btnAddStock() {
    let data = {
      sizes: Number(this.sizeFootwaerModal),
      total: Number(this.totalFotwaerModal)
    }
    this.arrayColor[this.position].stock.push(data);
    // console.log(this.arrayColor[this.position].stock);
  }
  // Eliminar dato de arreglos
  btnDeleteDataInArray(Array, index) {
    Array.splice(index, 1);
  }
  // Cargar imagenes
  onUploadChange(evt: any, flag?: boolean) {
    const file = evt.target.files;
    // console.log(flag);
    this.flagOfImgPrincipal = flag;
    if (file) {
      for (const iterator of file) {
        this.typeOfImg = iterator.type;

        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);

        reader.readAsBinaryString(iterator);
      }
    }
  }
  handleReaderLoaded(e) {
    let data = `data:${this.typeOfImg};base64,`;
    if(!this.flagOfImgPrincipal) {
      let obj = {
        id: 0,
        img: data + btoa(e.target.result),
        typeImg: `${this.firstFormGroup.get('number_folio').value}_${this.arrayColor[this.position].data.name}`
      }
      this.arrayColor[this.position].image.push(obj);
    } else {
      this.secondFormGroup.controls['img_principal'].setValue(data + btoa(e.target.result));
      // console.log(this.secondFormGroup.get('img_principal').value);
    }
  }
  // Eliminar imagenes
  btnDeleteImg(index) {
    this.arrayColor[this.position].image.splice(index, 1);
  }

  printColor() {
    console.log('Array de colores: ',this.arrayColor);
    console.log('Formulario 1: ', this.firstFormGroup.value);
    console.log('Formulario 2: ', this.secondFormGroup.value);
    console.log('Formulario 3: ', this.thirdFormGroup.value);
  }
}
