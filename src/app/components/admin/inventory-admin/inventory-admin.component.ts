import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-inventory-admin',
  templateUrl: './inventory-admin.component.html',
  styleUrls: ['./inventory-admin.component.css']
})
export class InventoryAdminComponent implements OnInit {

  flagInventory: boolean = false;

  constructor(private generalService: GeneralService) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = true;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
  }

  btnTypeOfAct(number: number) {
    if(number === 1) this.flagInventory = false;
    else this.flagInventory = true;
  }

}
