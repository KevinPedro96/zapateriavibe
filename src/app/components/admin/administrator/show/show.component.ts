import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { SuperUsersService } from 'src/app/services/super-users.service';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

  admins: any = [];
  @ViewChild('closeBtn') closeBtn: ElementRef;
  data: any = null;
  // showPassword: boolean = false;
  flagFormAdmin: boolean = false;
  typeOfTask: boolean; //si es false nuevo registro, si es true editar registro

  constructor(private generalService: GeneralService, private superUsersService: SuperUsersService) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = true;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
    this.getAdmins();
  }

  getAdmins() {
    this.superUsersService.getAdmins().subscribe(res => {
      this.admins = res;
    });
  }

  flagModal() {
    this.flagFormAdmin = !this.flagFormAdmin;
  }

  btnpassword(obj) {
    obj.psw = !obj.psw;
  }

  btnAddAdmin() {
    this.typeOfTask = false;
    this.flagFormAdmin = true;
  }

  btnEditAdmin(data: any) {
    this.data = data;
    this.typeOfTask = true;
    this.flagFormAdmin = true;
  }

  btnDeleteAdmin(id) {
    this.superUsersService.deleteAdmin(id).subscribe(res => {
      console.log(res);
      this.getAdmins();
    });
  }

  detonationRefresh(event: boolean) {
    if(event === true) {
      this.closeBtn.nativeElement.click();
      this.getAdmins();
    }
  }

}
