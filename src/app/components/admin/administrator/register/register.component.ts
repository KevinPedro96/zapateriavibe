import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SuperUsersService } from 'src/app/services/super-users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input() data: any;
  @Input() typeOfTask: boolean;
  @Output() active: EventEmitter<boolean>;

  formAdmin: FormGroup;
  hide: boolean = true;
  describeAdmin: string = '';
  typeOfAdmin = [
    {
      name: 'Administrador general',
      type: 'A',
      description: 'Tiene acceso a carrusel, inventario, pedidos, ventas y administrador.'
    },
    {
      name: 'Administrador local',
      type: 'B',
      description: 'Tiene acceso a carrusel, inventario, pedidos y ventas.'
    },
    {
      name: 'Administrador secundario',
      type: 'C',
      description: 'Tiene acceso a carrusel, invenrario y ventas'
    },
    {
      name: 'Administrador de inventario, pedidos y ventas',
      type: 'D',
      description: 'Tiene acceso a inventario, pedido y ventas.'
    },
    {
      name: 'Administrador de inventario y pedidos',
      type: 'E',
      description: 'Tiene acceso a inventario y ventas'
    },
    {
      name: 'Administrador de inventario',
      type: 'F',
      description: 'Tiene acceso a inventario'
    },
    {
      name: 'Administrador de pedidos',
      type: 'G',
      description: 'Tiene acceso a pedidos'
    },
    {
      name: 'Administrador de carrusel y pedidos',
      type: 'H',
      description: 'Tiene acceso a carrusel y pedidos'
    },
    {
      name: 'Administrador de carrusel e inventario',
      type: 'I',
      description: 'Tiene acceso a carrusel e inventario'
    },
    {
      name: 'Administrador de diseño',
      type: 'J',
      description: 'Tiene acceso al carrusel'
    }
  ]

  constructor(private superUsersService: SuperUsersService) {
    this.active = new EventEmitter();
  }
  
  ngOnInit(): void {
    this.typeOfAdmin = this.orderByTypeOfAdmin(this.typeOfAdmin);
    this.initialForm();
    if(this.typeOfTask) {
      console.log('data: ',this.data);
      this.loadForm(this.data);
    }
  }

  initialForm() {
    this.formAdmin = new FormGroup({
      id: new FormControl(null),
      name: new FormControl('', [Validators.required]),
      user_name: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required])
    });
  }

  loadForm(obj) {
    this.formAdmin.setValue({
      id: obj.id,
      name: obj.name,
      user_name: obj.user_name,
      password: obj.password,
      type: obj.type
    });
  }

  btnSave(formGroup: FormGroup) {
    if(formGroup.valid) {
      if(!this.typeOfTask) {
        this.superUsersService.addAdmin(formGroup.value).subscribe(res => {
          console.log(res);
          this.active.emit(true);
        });
      } else {
        this.superUsersService.editAdmin(formGroup.value).subscribe(res => {
          console.log(res);
          this.active.emit(true);
        });
        // console.log(formGroup.value);
      }
    } else {
      console.log('Falta información');
    }
  }

  orderByTypeOfAdmin(arrayAdmin: any) {
    return arrayAdmin.sort((a,b) => {
      if (a.name > b.name) return 1;
      if (a.name < b.name) return -1;
      return 0;
    });
  }

  descripcionOfAdmin(event?) {
    this.describeAdmin = this.typeOfAdmin.find(element => element.type == event).description;
  }

}
