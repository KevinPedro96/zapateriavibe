import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-orders-admin',
  templateUrl: './orders-admin.component.html',
  styleUrls: ['./orders-admin.component.css']
})
export class OrdersAdminComponent implements OnInit {

  constructor(private generalService: GeneralService) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = true;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
  }

}
