import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-sales-admin',
  templateUrl: './sales-admin.component.html',
  styleUrls: ['./sales-admin.component.css']
})
export class SalesAdminComponent implements OnInit {

  constructor(private generalService: GeneralService) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = true;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
  }

}
