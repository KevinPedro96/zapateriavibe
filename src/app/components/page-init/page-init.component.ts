import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-page-init',
  templateUrl: './page-init.component.html',
  styleUrls: ['./page-init.component.css']
})
export class PageInitComponent implements OnInit {

  constructor(private generalService: GeneralService) { }

  ngOnInit(): void {
  }

}
