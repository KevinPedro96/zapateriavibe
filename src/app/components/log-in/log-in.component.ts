import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { EncriptacionService } from 'src/app/services/encriptacion.service';
import { Router } from '@angular/router';
import { SuperUsersService } from 'src/app/services/super-users.service';

export class loginClass
{
  user_name: String;
  password: String;
}

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  formLogin: FormGroup;

  constructor(private generalService: GeneralService, private superUsersService: SuperUsersService, private encriptacionService: EncriptacionService, private router: Router) {
    this.generalService.flagNavbar = false;
    this.generalService.flagNavbarAdmin = false;
    this.generalService.flagFooter = false;
  }

  ngOnInit(): void {
    this.initialForm();
    localStorage.clear();
  }

  initialForm() {
    this.formLogin = new FormGroup({
      user_name: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  btnLogIn(formGroup: FormGroup) {
    if(formGroup.valid) {
      let newObj = new loginClass();
      newObj.user_name = btoa(formGroup.get('user_name').value);
      newObj.password = btoa(formGroup.get('password').value);
      this.superUsersService.login(newObj).subscribe(res => {
        // console.log(res);
        if(res !== null) {
          const user = this.encriptacionService.encrypt(JSON.stringify(res));
          localStorage.setItem(this.encriptacionService.profile, user);
          this.router.navigateByUrl('carrusel');
        } else {
          console.log('Datos incorrectos');
        }
      });
    }
  }

}
