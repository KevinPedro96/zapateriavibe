import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// Componentes de angular components library
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* Cdk */
import {CdkStepperModule} from '@angular/cdk/stepper';
/* Mat */
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from "@angular/material/select";
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';

// Componentes
import { NavbarComponent } from './components/navbar/navbar.component';
import { PageInitComponent } from './components/page-init/page-init.component';
import { FooterComponent } from './components/footer/footer.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { CarouselAdminComponent } from './components/admin/carousel-admin/carousel-admin.component';
import { InventoryAdminComponent } from './components/admin/inventory-admin/inventory-admin.component';
import { OrdersAdminComponent } from './components/admin/orders-admin/orders-admin.component';
import { SalesAdminComponent } from './components/admin/sales-admin/sales-admin.component';
import { RegisterComponent } from './components/admin/administrator/register/register.component';
import { ShowComponent } from './components/admin/administrator/show/show.component';
import { StatusPedidoComponent } from './components/status-pedido/status-pedido.component';
import { RegisterPorductComponent } from './components/admin/inventory-admin/register-porduct/register-porduct.component';
import { ShowPorductsComponent } from './components/admin/inventory-admin/show-porducts/show-porducts.component';

import { DamaComponent } from './components/dama/dama.component';
import { NinaComponent } from './components/nina/nina.component';
import { MayoristasComponent } from './components/mayoristas/mayoristas.component';

const angularComponents = [
  // Angular Material SDK
  CdkStepperModule,
  // Angular Material MAT
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  DragDropModule,
  MatStepperModule,
  MatAutocompleteModule,
  MatExpansionModule,
  MatTabsModule
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PageInitComponent,
    FooterComponent,
    LogInComponent,
    CarouselAdminComponent,
    InventoryAdminComponent,
    OrdersAdminComponent,
    SalesAdminComponent,
    RegisterComponent,
    ShowComponent,
    StatusPedidoComponent,
    RegisterPorductComponent,
    ShowPorductsComponent,
    DamaComponent,
    NinaComponent,
    MayoristasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    // Angular Material
    BrowserAnimationsModule,
    angularComponents
  ],
  exports: [
    angularComponents
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
