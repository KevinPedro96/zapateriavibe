import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  readonly URL_API = `${Global.host}:${Global.port}/api`;

  constructor(private http: HttpClient) { }

  getDataGeneral() {
    return this.http.get(`${this.URL_API}/general/data`);
  }

  getDataStock(number_folio) {
    return this.http.get(`${this.URL_API}/general/stock/${number_folio}`);
  }
}
