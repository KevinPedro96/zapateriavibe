import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class EncriptacionService {

  key: string = 'Un gran poder conlleva una gran responsabilidad by Peter Parker c:';
  profile: string;

  constructor() {
    this.profile = this.encryptBase64('profile');
  }

  encrypt(text: string): string {
    const encryptText = CryptoJS.AES.encrypt(text, this.key.trim()).toString();
    return encryptText;
  }

  decrypt(text: string): string {
    const decryptText = CryptoJS.AES.decrypt(text, this.key.trim()).toString(CryptoJS.enc.Utf8);
    return decryptText;
  }

  encryptBase64(text: string): string {
    const data = btoa(text);
    return data;
  }

  decryptBase64(text: string): string {
    const data = atob(text);
    return data;
  }
}
