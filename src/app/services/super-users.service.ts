import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
export class SuperUsersService {

  readonly URL_API = `${Global.host}:${Global.port}/api/superUsers`;
  
  constructor(private http: HttpClient) { }

  getAdmins() {
    return this.http.get(this.URL_API);
  }

  deleteAdmin(id: number) {
    return this.http.get(`${this.URL_API}/delete/${id}`);
  }

  addAdmin(obj: any) {
    return this.http.post(this.URL_API, obj);
  }

  editAdmin(obj: any) {
    return this.http.post(`${this.URL_API}/edit`, obj);
  }

  login(obj: any) {
    return this.http.post(`${this.URL_API}/login`, obj);
  }
}
