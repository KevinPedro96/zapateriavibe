import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Global } from './global';


@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  readonly URL_API = `${Global.host}:${Global.port}/api`;
  flagNavbar: boolean;
  flagNavbarAdmin: boolean;
  flagFooter: boolean;
  
  constructor(private http: HttpClient) {
    this.flagFooter = true;
    this.flagNavbar = true;
    this.flagNavbarAdmin = false;
  }

  addImg(img: any) {
    return this.http.post(`${this.URL_API}/imagen`, img);
  }

  editPosition(img: any) {
    return this.http.post(`${this.URL_API}/imagen/position`, img);
  }

  getImgs(type: string) {
    return this.http.get(`${this.URL_API}/imagen/${type}`);
  }

  deleteImg(id: number) {
    return this.http.get(`${this.URL_API}/imagen/delete/${id}`);
  }
  
}
